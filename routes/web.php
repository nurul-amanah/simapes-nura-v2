<?php

use App\Http\Livewire\Santri\Semua;
use App\Http\Livewire\Santri\Tambah;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function (){
   Route::group(['prefix' => 'santri'], function (){
       Route::get('semua', Semua::class)->name('santri.semua');
       Route::get('santri-baru', Tambah::class)->name('santri.tambah');
   });
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
