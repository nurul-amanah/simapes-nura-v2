<?php

namespace App\Http\Livewire\Santri;

use Livewire\Component;

class Tambah extends Component
{
    public $nama_lengkap_santri, $nama_panggilan_santri, $nisn, $npsn, $jenis_kelamin, $tempat_lahir, $tanggal_lahir, $anak_ke,
    $jumlah_saudara, $status_dalam_keluarga, $alamat, $jenjang_pendidikan, $status_siswa, $asal_sekolah, $alamat_sekolah, $validasi;
    public function render()
    {
        return view('livewire.santri.tambah');
    }
}
