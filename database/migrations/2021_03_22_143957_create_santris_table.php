<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSantrisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('santris', function (Blueprint $table) {
            $table->id();
            $table->string('nama_lengkap', 100);
            $table->string('nama_panggilan', 100)->nullable();
            $table->string('nisn', 30)->nullable()->default('Belum ada')->unique();
            $table->string('npsn', 30)->nullable()->default('Belum ada');
            $table->string('jenis_kelamin', 100);
            $table->string('tempat_lahir', 100);
            $table->string('tanggal_lahir', 100);
            $table->string('anak_ke', 2);
            $table->string('jumlah_saudara', 2);
            $table->string('status_dalam_keluarga', 100);
            $table->text('alamat');
            $table->string('jenjang_pendidikan', 100);
            $table->string('status_siswa', 100);
            $table->string('asal_sekolah', 100);
            $table->text('alamat_sekolah');
            $table->enum('validasi', ['sudah', 'belum']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('santris');
    }
}
