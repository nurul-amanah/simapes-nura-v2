<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAyahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ayahs', function (Blueprint $table) {
            $table->id();
            $table->string('nama_lengkap', );
            $table->string('tempat_lahir', );
            $table->string('tanggal_lahir', );
            $table->string('agama', );
            $table->string('pendidikan_terakhir', )->nullable()->default('Belum diisi');
            $table->string('pekerjaan', );
            $table->text('alamat');
            $table->string('penghasilan', );
            $table->string('telepon', 24);
            $table->foreignId('santri_id')->constrained()->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ayahs');
    }
}
