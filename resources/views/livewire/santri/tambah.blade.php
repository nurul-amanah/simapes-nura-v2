<div>
    {{-- Close your eyes. Count to one. That is how long forever feels. --}}
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">Tambah Santri</h3>
                <div class="nk-block-des text-soft">
                    <p>Silahkan masukan data santri yang mau di tambahkan</p>
                </div>
            </div><!-- .nk-block-head-content -->

        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="card">
            <div class="card-inner">
                <fieldset class="border p-3 mb-2">
                    <legend class="w-auto">
                        <p class="text-primary" style="font-size: 18px">Biodata Santri</p>
                    </legend>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="kk">Nomor Kartu Keluarga(KK):</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <input  wire:model="kartu_keluarga"type="text" class="form-control" id="kartu_keluarga" placeholder="8988877xxxxxxxx"">
                                </div>
                            </div>
                        </div>
                        <div class=" col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="ktp">Nomor Induk Keluarga (KTP):</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <input wire:model="ktp" type="text" class="form-control" id="ktp"
                                           placeholder="8988877xxxxxxxx">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 mt-2 ">
                            <div class="form-group">
                                <label class="form-label" for="nama-lengkap">Nama Lengkap:</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <input wire:model="nama_lengkap" type="text" class="form-control" id="nama-lenkap" name="nama-lengkap"
                                           placeholder="Ahmad Zain">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 mt-2 ">
                            <div class="form-group">
                                <label class="form-label" for="nama_panggilan">Nama Panggilan:</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <input wire:model="nama_panggilan" type="text" class="form-control" id="nama_panggilan"
                                           name="nama-panggilan" placeholder="Ahmad">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 mt-2 ">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label" for="tempat_lahir">Tempat Lahir</label>
                                        <div class="form-control-wrap">
                                            <input  wire:model="tempat_lahir" type="text" class="form-control" id="tempat_lahir"
                                                   placeholder="Masukan Nama Kota">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label class="form-label" for="tanggal_lahir">Tanggal Lahir:</label>
                                        <div class="form-control-wrap">
                                            <input wire:model="tangal_lahir" type="date" class="form-control" id="tanggal_lahir"
                                                   name="nama-panggilan">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 mt-2 ">
                            <div class="form-group">
                                <label class="form-label" for="jenis-kelamin">Jenis Kelamin:</label>
                                <div class="g-4 align-center flex-wrap">
                                    <div class="g">
                                        <div class="custom-control custom-control-sm custom-radio">
                                            <input wire:model="laki-laki" type="radio" class="custom-control-input" name="laki-laki"
                                                   id="laki-laki">
                                            <label class="custom-control-label"
                                                   for="laki-laki">Laki-Laki</label>
                                        </div>
                                        <div class="custom-control custom-control-sm custom-radio ml-3">
                                            <input wire:model="perempuan" type="radio" class="custom-control-input" name="perempuan"
                                                   id="perempuan">
                                            <label class="custom-control-label"
                                                   for="perempuan">Perempuan</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 mt-2 ">
                            <div class="form-group">
                                <label class="form-label" for="jumlah-saudara">Jumlah Saudara:</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <input wire:model="jumlah_saudara" type="text" class="form-control" id="jumlah_saudara"
                                           name="jumlah_saudara" placeholder="2">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 mt-2 ">
                            <div class="form-group">
                                <label class="form-label" for="anak-ke">Anak Ke:</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <input wire:model="anak_ke" type="text" class="form-control" id="anak_ke"
                                           name="anak_ke" placeholder="2">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 mt-2 ">
                            <div class="form-group">
                                <label class="form-label" for="status_keluarga">Status Keluarga:</label>
                                <select wire:model="status_keluarga" class="form-select">
                                    <option value="option_select_name" data-select2-id="status_keluagra1">--- Isikan Status
                                        Keluarga
                                        ---
                                    </option>
                                    <option value="option_select_name" data-select2-id="status_keluagra2">Anak Kandung
                                    </option>
                                    <option value="option_select_name" data-select2-id="status_keluagra3">Anak Tiri</option>
                                    <option value="option_select_name" data-select2-id="status_keluagra4">Anak Angkat</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 mt-2 ">
                            <div class="form-group">
                                <label class="form-label" for="jumlah_saudara">Jumlah Saudara:</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <input wire:model="jumlah_saudara" type="number" class="form-control" id="jumlah_saudara"
                                           name="jumlah_saudara" placeholder="2">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 mt-2 ">
                            <div class="form-group">
                                <label class="form-label" for="anak_ke">Anak Ke:</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <input wire:model="anak_ke" type="number" class="form-control" id="anak_ke"
                                           name="nama-panggilan" placeholder="2">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 mt-2 ">
                            <div class="form-group">
                                <label class="form-label" for="tinggal_bersama">Tinggal Bersama:</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <select wire:model="tinggal_bersama" class="form-select">
                                        <option value="option_select_name" data-select2-id="230">--- Isikan Status
                                            Tinggal
                                            ---
                                        </option>
                                        <option value="option_select_name" data-select2-id="tinggal_bersama">Orang Tua
                                        </option>
                                        <option value="option_select_name" data-select2-id="tinggal_bersama1">Kakek/Nenek</option>
                                        <option value="option_select_name" data-select2-id="tinggal_bersama2">Lainnya</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 mt-2 ">
                            <div class="form-group">
                                <label class="form-label" for="alamat">Alamat:</label>
                                <div class="form-control-wrap">
                                    <textarea wire:model="alamat" class="form-control" id="alamat" name="alamat"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 mt-2 ">
                            <div class="form-group">
                                <label class="form-label" for="status_masuk">Status Masuk</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <select wire:model="status_masuk" class="form-select">
                                        <option value="option_select_name" data-select2-id="230">--- Pilih
                                            Pendaftaraan Masuk
                                            ---
                                        </option>
                                        <option value="option_select_name" data-select2-id="status_masuk">Pondok Pesantren
                                        </option>
                                        <option value="option_select_name" data-select2-id="status_masuk1">Formal/Sekolah</option>
                                        <option value="option_select_name" data-select2-id="status_masuk2">Lainnya</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 mt-2 ">
                            <div class="form-group">
                                <label class="form-label" for="jenjang_pendidikan">Jenjang Pendidikan</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <select wire:model="jenjang_pendidikan" class="form-select">
                                        <option value="option_select_name" data-select2-id="230">--- Pilih Jenjang
                                            Pendidikan ---
                                        </option>
                                        <option value="option_select_name" data-select2-id="jenjang_pendidikan">SMP Nurul Amanah
                                        </option>
                                        <option value="option_select_name" data-select2-id="jenjang_pendidikan1">MTs Nurul Amanah
                                        </option>
                                        <option value="option_select_name" data-select2-id="jenjang_pendidikan2">SMA Nurul Amanah
                                        </option>
                                        <option value="option_select_name" data-select2-id="jenjang_pendidikan3">SMK Nurul Amanah
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 mt-2 ">
                            <div class="form-group">
                                <label class="form-label" for="jurusan">Jurusan</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <select wire:model="jurusan" class="form-select">
                                        <option value="option_select_name" data-select2-id="jurusan">--- Pilih Jurusan ---
                                        </option>
                                        <option value="option_select_name" data-select2-id="jurusan1">Teknik Komputer
                                            Jaringan
                                        </option>
                                        <option value="option_select_name" data-select2-id="jurusan2">Teknik Sepeda Motor
                                        </option>
                                        <option value="option_select_name" data-select2-id="jurusan3">Teknik Akuntansi
                                        </option>
                                    </select>
                                    <small><small><p><sup>*</sup>Jurusan Hanya Di diisi untuk SMK</p></small></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 mt-2 ">
                            <div class="form-group">
                                <label class="form-label" for="status_siswa">Status Siswa</label>
                                <div class="form-control-wrap">
                                    <select wire:model="status_siswa" class="form-select">
                                        <option value="option_select_name" data-select2-id="230">--- Pilih Status Siswa
                                            ---
                                        </option>
                                        <option value="option_select_name" data-select2-id="28">Baru</option>
                                        <option value="option_select_name" data-select2-id="28">Pindahan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 mt-2 ">
                            <div class="form-group">
                                <label class="form-label" for="asal_sekolah">Asal Sekolah</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-university"></em>
                                    </div>
                                    <input wire:model="asal_sekolah" type="text" class="form-control" id="asal_sekolah"
                                           placeholder="SMK Surabaya 9">

                                </div>
                                <small><p><sup>*</sup>Diisi jika siswa pindahan</p></small>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 mt-2 ">
                            <div class="form-group">
                                <label class="form-label" for="alamat_sekolah">Alamat Sekolah</label>
                                <div class="form-control-wrap">
                                    <textarea wirel:model="alamat_sekolah" class="form-control" id="alamat_sekolah"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="foto_unggah">Unggah Foto Anda</label>
                                <div class="form-control-wrap">
                                    <div class="custom-file">
                                        <input wire:model="foto_unggah" type="file" multiple="" class="custom-file-input" id="customFile">
                                        <label class="custom-file-label" for="foto_unggah">Pilih Berkas Foto</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>

                {{--SECTION BIODATA Ayah--}}
                <fieldset class="border p-3 mb-2 mt-5">
                    <legend class="w-auto"><p class="text-primary" style="font-size: 18px">Biodata Ayah</p></legend>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="kk">Nama Lengkap</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <input wire:model="nama_lengkap_ibu" type="text" class="form-control" id="kk" name="kk" placeholder="Masukan Nama Ayah">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="kk">Nomor Telepon</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-call-alt"></em>
                                    </div>
                                    <input wire:model="telepon_ibu" type="text" class="form-control" id="kk" name="kk" placeholder="Masukan Nomor Telepon">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="kk">Tempat Lahir</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <input wire:model="tempat_lahir_ibu" type="text" class="form-control" id="kk" name="kk" placeholder="Masukan Nama Ayah">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="kk">Tanggal Lahir</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-calender-date"></em>
                                    </div>
                                    <input wire:model="tanggal_lahir_ibu" type="date" class="form-control" id="kk" name="kk" placeholder="Masukan Nomor Telepon">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="kk">Agama</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <select wire:model="agama_ibu" name="" class="form-select" id="" data-placeholder="Pilih Agama">
                                        <option value="">-- Pilih Agama --</option>
                                        <option value="Islam">Islam</option>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                        <option value="Katolik">Katolik</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="kk">Pendidikan Terakhir</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <select wire:model="pendidikan_terakhir_ibu" name="" class="form-select" id="" data-placeholder="Pilih Pendidikan Terakhir">
                                        <option value="">-- Pilih Jenjang Pendidikan --</option>
                                        <option value="SD/MI">SD/MI</option>
                                        <option value="SMP/Sederajat">SMP/Sederajat</option>
                                        <option value="SMA/Sederajat">SMA/Sederajat</option>
                                        <option value="S1/Sederajat">S1/Sederajat</option>
                                        <option value="S2/Sederajat">S2/Sederajat</option>
                                        <option value="S3/Sederajat">S3/Sederajat</option>
                                        <option value="Lainnya">Lainnya</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="kk">Pekerjaan</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <select wire:model="pekerjaan_ibu" name="" class="form-select" id="" data-placeholder="Pilih Pekerjaan">
                                        <option value="">-- Pilih Pekerjaan --</option>
                                        <option value="Petani/Pekebun">Petani/Pekebun</option>
                                        <option value="Nelayan">Nelayan</option>
                                        <option value="Guru">Guru</option>
                                        <option value="Pegawai Negeri Sipil">Pegawai Negeri Sipil</option>
                                        <option value="Pegawai Swasta">Pegawai Swasta</option>
                                        <option value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>
                                        <option value="Lainnya">Lainnya</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="kk">Penghasilan</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <select wire:model="penghasilan_ibu" name="" class="form-select" id="" data-placeholder="Pilih Penghasilan">
                                        <option value="">-- Pilih Pekerjaan --</option>
                                        <option value="< Rp. 500.000 s/d 1.0000.000"> < Rp. 500.000 s/d 1.0000.000</option>
                                        <option value="1.000.000 s/d 5.000.000">1.000.000 s/d 5.000.000</option>
                                        <option value="5.000.000 s/d 10.000.000">5.000.000 s/d 10.000.000</option>
                                        <option value="Lainnya">Lainnya</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group mt-2">
                        <label class="form-label" for="kk">Alamat Lengkap</label>
                        <div class="form-control-wrap">
                            <textarea wire:model="alamat_ibu" name="" id="" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                    </div>
                </fieldset>

                {{--SECTION BIODATA IBU--}}
                <fieldset class="border p-3 mb-2 mt-5">
                    <legend class="w-auto"><p class="text-primary" style="font-size: 18px">Biodata Ibu</p></legend>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="kk">Nama Lengkap</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <input wire:model="nama_lengkap_ibu" type="text" class="form-control" id="kk" name="kk" placeholder="Masukan Nama Ayah">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="kk">Nomor Telepon</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-call-alt"></em>
                                    </div>
                                    <input wire:model="telepon_ibu" type="text" class="form-control" id="kk" name="kk" placeholder="Masukan Nomor Telepon">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="kk">Tempat Lahir</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <input wire:model="tempat_lahir_ibu" type="text" class="form-control" id="kk" name="kk" placeholder="Masukan Nama Ayah">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="kk">Tanggal Lahir</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-calender-date"></em>
                                    </div>
                                    <input wire:model="tanggal_lahir_ibu" type="date" class="form-control" id="kk" name="kk" placeholder="Masukan Nomor Telepon">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="kk">Agama</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <select wire:model="agama_ibu" name="" class="form-select" id="" data-placeholder="Pilih Agama">
                                        <option value="">-- Pilih Agama --</option>
                                        <option value="Islam">Islam</option>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                        <option value="Katolik">Katolik</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="kk">Pendidikan Terakhir</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <select wire:model="pendidikan_terakhir_ibu" name="" class="form-select" id="" data-placeholder="Pilih Pendidikan Terakhir">
                                        <option value="">-- Pilih Jenjang Pendidikan --</option>
                                        <option value="SD/MI">SD/MI</option>
                                        <option value="SMP/Sederajat">SMP/Sederajat</option>
                                        <option value="SMA/Sederajat">SMA/Sederajat</option>
                                        <option value="S1/Sederajat">S1/Sederajat</option>
                                        <option value="S2/Sederajat">S2/Sederajat</option>
                                        <option value="S3/Sederajat">S3/Sederajat</option>
                                        <option value="Lainnya">Lainnya</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="kk">Pekerjaan</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <select wire:model="pekerjaan_ibu" name="" class="form-select" id="" data-placeholder="Pilih Pekerjaan">
                                        <option value="">-- Pilih Pekerjaan --</option>
                                        <option value="Petani/Pekebun">Petani/Pekebun</option>
                                        <option value="Nelayan">Nelayan</option>
                                        <option value="Guru">Guru</option>
                                        <option value="Pegawai Negeri Sipil">Pegawai Negeri Sipil</option>
                                        <option value="Pegawai Swasta">Pegawai Swasta</option>
                                        <option value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>
                                        <option value="Lainnya">Lainnya</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="kk">Penghasilan</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-left">
                                        <em class="icon ni ni-user"></em>
                                    </div>
                                    <select wire:model="penghasilan_ibu" name="" class="form-select" id="" data-placeholder="Pilih Penghasilan">
                                        <option value="">-- Pilih Pekerjaan --</option>
                                        <option value="< Rp. 500.000 s/d 1.0000.000"> < Rp. 500.000 s/d 1.0000.000</option>
                                        <option value="1.000.000 s/d 5.000.000">1.000.000 s/d 5.000.000</option>
                                        <option value="5.000.000 s/d 10.000.000">5.000.000 s/d 10.000.000</option>
                                        <option value="Lainnya">Lainnya</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group mt-2">
                        <label class="form-label" for="kk">Alamat Lengkap</label>
                        <div class="form-control-wrap">
                            <textarea wire:model="alamat_ibu" name="" id="" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                    </div>
                </fieldset>

                <div class="mt-2">
                    <button class="btn btn-success float-right"><em class="icon ni ni-save mr-1"></em>Simpan Santri</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

